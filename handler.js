const fs = require('fs');


const fetchProducts = () => {
    try {
        const products = fs.readFileSync('products.json');
        return JSON.parse(products);
    } catch (e) {
        console.log(e);
        return [];
    }
};

const saveProducts = (products) => {
    fs.writeFileSync('products.json', JSON.stringify(products));
}


const addProduct = (key, value) => {
    let products = [];
    let product = { key, value };
    products = fetchProducts();
    const duplicated = products.find((product) => {
        return product.key === key;
    });
    if(!duplicated) {
        products.push(product);
        saveProducts(products);
        return true;
    }
    return;
};

const listProducts = () => {
    const products = fetchProducts();
    return products;
};

const getProduct = (key) => {
    const products = fetchProducts();
    const product = products.find((product) => {
        return product.key === key;
    });
    if(product) {
        return product;
    }
    return ;
};

const removeProduct= (key) => {
    const products = fetchProducts();
    const filteredProducts = products.filter((product) => {
        return product.key !== key;
    });
    saveProducts(filteredProducts);
    const isDeleted = products.length !== filteredProducts.length;
    if(isDeleted) {
        return true;
    }
    return;
};

const clearAll = () => {
    saveProducts([]);
    const isRemoved = fetchProducts().length === 0;
    if(isRemoved) {
        return true;
    }
    return;
}


module.exports = {
    addProduct,
    listProducts,
    getProduct,
    removeProduct,
    clearAll
}








