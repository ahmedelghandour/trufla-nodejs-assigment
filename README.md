## Simple JS/Node Developer Challenge


### Store Commands

`$ node store.js add --key="mykey" --value="myvalue"`  or  `$ node store.js add -k="mykey" -v="myvalue"`

`$ node store.js list`

`$ node store.js get --key="mykey"`  or  `$ node store.js get -k="mykey"`

`$ node store.js remove --key="mykey"`  or  `$ node store.js get -k="mykey"`

`$ node store.js clear`

