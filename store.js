

const handler = require('./handler.js');
const yargs = require('yargs');


const argv = yargs
    .command('add', 'add new product', {
        key: {
            describe: 'Key of the product',
            demand: true,
            alias: 'k'
        },
        value: {
            describe: 'Value of the product',
            demand: true,
            alias: 'v'
        }
    })
    .command('list', 'list all products',{})
    .command('get', 'get a product', {
        key: {
            describe: 'Key of the product',
            demand: true,
            alias: 'k'
        }
    })
    .command('remove', 'remove a product', {
        key: {
            describe: 'Key of the product',
            demand: true,
            alias: 'k'
        }
    })
    .command('clear', 'clear all products', {})
    .argv;

    const command = argv._[0];
    
    /////////////////////////////////////////////////////////////////////

    if(command === 'add') {
        const data = handler.addProduct(argv.key, argv.value);
        // console.log(x);
        if(data) {
            console.log('Congratulation!. your product is successfully added.');
            console.log('-----------------------');
            console.log(`key: ${argv.key}, value: ${argv.value}`);
        } else {
            console.log('Sorry, this product is aready available! try to put a new product');
        }
    } else if(command === 'list') {
        const data = handler.listProducts();
        if(data.length !== 0) {
            console.log('All Available Products in the store');
            console.log('-----------------------');
            data.forEach(elm => {   
            console.log(`key: ${elm.key}, value: ${elm.value}`);
            });
        } else {
            console.log('unfortunately, no available products at this moment, please come later');
        }
    } else if(command === 'get') {
        const data = handler.getProduct(argv.key);
        if(data) {
            console.log('product is available! here is it.');
            console.log('-----------------------');
            console.log(`key: ${data.key}, value: ${data.value}`);
        } else {
            console.log('Sorry! this product is not available at the store.');   
        }
    } else if(command === 'remove') {
        const data = handler.removeProduct(argv.key);
        if(data) {
            console.log(`you Product "${argv.key}" has successfully removed.`);
        } else {
            console.log('sorry, this product is not available in the store to remove it!');
        }
    } else  if(command === 'clear') {
        const data = handler.clearAll();
        if(data) {
            console.log('All products successfully removed');
        } else {
            console.log('Something went wrong! please try again later');
        }
    } else {
        console.log('Sorry! command is not correct');
    }
    